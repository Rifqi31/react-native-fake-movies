import React from 'react';
import { StyleSheet, View } from 'react-native';
// import app route
import AppRoute from './src/route/appRoute';

export default class App extends React.Component {

	// call the route application
	render() {
		return (
			<AppRoute />
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
