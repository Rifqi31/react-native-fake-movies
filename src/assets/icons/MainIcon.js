// import react module
import React from 'react';
// import react native module
import { View, Image } from 'react-native';
// import proptypes for define prop data type
import PropTypes from 'prop-types';
// import icons asset
import * as iconsHelper from './index';
// Main Class
export default class MainIcon extends React.Component {
  //  create arrow function for icon customize
  iconCondition = () => {
    // deconstruction props variable
    const {
      iconType, iconSize, iconColor, colorOverride,
    } = this.props;
    // create label for each icons
    const containIconType = [
      'discover', 'browse', 'search',
      'library', 'play-video', 'pistol', 'cowboy',
      'owl', 'theatre', 'knife-murder',
      'landscape', 'curtain', 'family',
      'castle', 'paper-scroll', 'skeleton',
      'disco-ball', 'masquerade', 'rose-flower',
      'small-alien-spaceship', 'watching-tv', 'chainsaw',
      'helmet', 'sheriff',
    ];
    const containIconName = [
      // simple icon
      iconsHelper.DiscoverIcon, iconsHelper.BrowseIcon,
      iconsHelper.SearchIcon, iconsHelper.LibraryIcon,
      iconsHelper.PlayVideoIcon,
      // colored icon
      iconsHelper.PistolGun, iconsHelper.HatCowboy,
      iconsHelper.OwlBird, iconsHelper.TheatreFace,
      iconsHelper.KnifeBlood, iconsHelper.TravelLandscape,
      iconsHelper.CurtainThing, iconsHelper.FamilySimple,
      iconsHelper.CastleBuild, iconsHelper.PaperScroll,
      iconsHelper.BoneSkeleton, iconsHelper.DiscoBall,
      iconsHelper.MasqueradeMask, iconsHelper.RoseFlower,
      iconsHelper.SmallSpaceShip, iconsHelper.TvScreen,
      iconsHelper.ChainSawBlade, iconsHelper.ArmyHelmet,
      iconsHelper.SherrifGuy,
    ];
    // this varibel for contain value from condition
    let containType;

    // first loop the icon images
    for (let countIconName = 0; countIconName < containIconName.length; countIconName += 1) {
      for (let countIconType = 0;
        countIconType < containIconType.length; countIconType += 1) {
        if (iconType === containIconType[countIconType]) {
          containType = containIconName[countIconType];
        }
      }
    }

    // conditional props color overwriten if false
    if (colorOverride === false) {
      return (
        <Image
          source={containType}
          style={{
            width: iconSize,
            height: iconSize,
          }}
        />
      );
    }
    // if you choice true boolean
    return (
      <Image
        source={containType}
        style={{
          width: iconSize,
          height: iconSize,
          tintColor: iconColor,
        }}
      />
    );
  };

  render() {
    return (
      <View>
        {/* Component in here */}
        { this.iconCondition() }
      </View>
    );
  }
}

// set props data type
MainIcon.propTypes = {
  iconType: PropTypes.string,
  iconSize: PropTypes.number,
  iconColor: PropTypes.string,
  colorOverride: PropTypes.bool,
};

// set default value for props
MainIcon.defaultProps = {
  iconType: 'none',
  iconSize: 0,
  iconColor: '#000',
  colorOverride: true,
};
