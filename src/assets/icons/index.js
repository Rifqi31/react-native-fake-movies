// import icon image
// basic icon
export const DiscoverIcon = require('./images/cell-phone-vibration.png');
export const BrowseIcon = require('./images/book.png');
export const SearchIcon = require('./images/search.png');
export const LibraryIcon = require('./images/heart.png');
export const PlayVideoIcon = require('./images/play-button.png');
// colored icon
export const PistolGun = require('./images/pistol.png');
export const HatCowboy = require('./images/hat.png');
export const OwlBird = require('./images/owl.png');
export const TheatreFace = require('./images/theatre.png');
export const KnifeBlood = require('./images/knife.png');
export const TravelLandscape = require('./images/travel.png');
export const CurtainThing = require('./images/curtain.png');
export const FamilySimple = require('./images/family.png');
export const CastleBuild = require('./images/castle.png');
export const PaperScroll = require('./images/paper-scroll.png');
export const BoneSkeleton = require('./images/skeleton.png');
export const DiscoBall = require('./images/disco-ball.png');
export const MasqueradeMask = require('./images/masquerade.png');
export const RoseFlower = require('./images/rose.png');
export const SmallSpaceShip = require('./images/spaceship.png');
export const TvScreen = require('./images/watching-tv.png');
export const ChainSawBlade = require('./images/chainsaw.png');
export const ArmyHelmet = require('./images/helmet.png');
export const SherrifGuy = require('./images/sheriff.png');
