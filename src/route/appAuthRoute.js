import React from 'react';
// import root native base
import { Root } from 'native-base';
// import react navigation
import { createStackNavigator, createAppContainer } from 'react-navigation';
// import app view
import LoginAppView from '../views/appView/loginPage';
import RegisterAppView from '../views/appView/registerPage';

// create object for stacknavigator
const appStackNavigator = createStackNavigator({
    loginPage_menu : { screen : LoginAppView },
    registerPage_menu : { screen : RegisterAppView }
}, {
    initialRouteName : 'loginPage_menu',
    headerMode : 'none'
});

// create object for container app navigator
const AppContainer = createAppContainer(appStackNavigator);

export default () =>
	<Root>
		<AppContainer />
	</Root>;
