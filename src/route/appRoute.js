import React from 'react';
// import root native base
import { Root } from 'native-base';
// import react navigation
import { createBottomTabNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
// import app view
import HomePageAppView from '../views/appViews/homePage';
import DetailMoviePageAppView from '../views/appViews/detailMovie';
import SearchMoviePageAppView from '../views/appViews/searchMovie';
import BrowseMoviePageAppView from '../views/appViews/browseMovie';
import DetailBrowseMoviePageAppView from '../views/appViews/detailBrowseMovie';
import ProfileUserAppView from '../views/appViews/profileUser';
// import custom tab bar navigation
import footerTab from '../views/components/footerTab';

// create object for stacknavigator
// home menu
const appHomeStackNav = createSwitchNavigator({
  homePage_menu: { screen: HomePageAppView },
  detailPage_menu: { screen: DetailMoviePageAppView },
  searchPage_menu: { screen: SearchMoviePageAppView },
}, {
  initialRouteName: 'homePage_menu',
  headerMode: 'none',
});

// browse menu
const appBrowseStackNav = createSwitchNavigator({
  browsePage_menu: { screen: BrowseMoviePageAppView },
  detailBrowsePage_menu: { screen: DetailBrowseMoviePageAppView },
}, {
  initialRouteName: 'browsePage_menu',
  headerMode: 'none',
});

// profle menu
const appProfileStackNav = createSwitchNavigator({
  profilePage_menu: { screen: ProfileUserAppView },
}, {
  initialRouteName: 'profilePage_menu',
  headerMode: 'none',
});

// create tab navigation
const appMainTabNav = createBottomTabNavigator({
  Discover: appHomeStackNav,
  Browse: appBrowseStackNav,
  library: appProfileStackNav,
}, {
  // overide tab component
  tabBarComponent: footerTab,
});

// create object for container app navigator
const AppContainer = createAppContainer(appMainTabNav);

export default () => (
  <Root>
    <AppContainer />
  </Root>
);
