import React from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
// import native base module
import {
  Header,
  // base component
  Container,
  Content,
  Button,
  ListItem,
  Card,
  CardItem,
} from 'native-base';
// import proptypes for define prop data type
import PropTypes from 'prop-types';
// import custom icon
import MainIcon from '../../assets/icons/MainIcon';
// import icons asset
// import * as iconsHelper from '../../assets/icons/index';

export default class browseMovie extends React.Component {
  //   create constructor
  constructor(props) {
    super(props);
    // default statement
    this.state = {
      data: [],
      // depends on API
      // using API TmDB API
      apiVersion: 3,
      apiKey: '9a4a662a126525b07d4b84b079d809d8',
      language: 'en-US',
    };
  }

  // calling default font for native base config
  componentDidMount() {
    // request tag genres
    this.makeRemoteRequestTagGenreMovies();
  }

  // request genres of movies
  makeRemoteRequestTagGenreMovies = () => {
    const { apiVersion, apiKey, language } = this.state;
    const url = `https://api.themoviedb.org/${apiVersion}/genre/movie/list?api_key=${apiKey}&language=${language}`;
    fetch(url)
      .then(response => response.json())
      .then((response) => {
        this.setState({
          // depends on json structure if there is no results field use response only
          data: response.genres,
        });
      });
  };

  renderItem = ({ item }) => {
    // destruction props
    const { navigation } = this.props;
    // create contain Icon which used in looping
    let containIcon;
    const listMenu = [
      'Action', 'Adventure', 'Animation', 'Comedy',
      'Crime', 'Documentary', 'Drama', 'Family',
      'Fantasy', 'History', 'Horror', 'Music',
      'Mystery', 'Romance', 'Science Fiction', 'TV Movie',
      'Thriller', 'War', 'Western',
    ];
    const iconTypeList = [
      'pistol', 'cowboy', 'owl', 'theatre', 'knife-murder', 'landscape',
      'curtain', 'family', 'castle', 'paper-scroll', 'skeleton', 'disco-ball',
      'masquerade', 'rose-flower', 'small-alien-spaceship',
      'watching-tv', 'chainsaw', 'helmet', 'sheriff',
    ];

    for (let indexIcon = 0; indexIcon < iconTypeList.length; indexIcon += 1) {
      for (let indexMenu = 0; indexMenu < listMenu.length; indexMenu += 1) {
        if (item.name === listMenu[indexMenu]) {
          containIcon = (
            <MainIcon
              iconType={iconTypeList[indexMenu]}
              iconSize={120}
              colorOverride={false}
            />
          );
        }
      }
    }

    return (
      <ListItem style={{ borderBottomWidth: 0 }}>
        {/* sending id genre only */}
        <TouchableOpacity onPress={() => navigation.navigate('detailBrowsePage_menu', { id: item.id })}>
          <Card>
            {containIcon}
            <CardItem>
              <Text>{ item.name }</Text>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </ListItem>
    );
  }

  render() {
    // destruction 'navigation' props
    const { navigation } = this.props;
    // destruction state
    const { data } = this.state;
    return (
      <Container>
        <Header androidStatusBarColor="#151A46" style={{ backgroundColor: '#151A46' }}>
          {/* Text Title */}
          <View style={{
            flex: 1,
            justifyContent: 'flex-start',
            flexDirection: 'row',
          }}
          />
          <View style={{
            marginVertical: 10,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
          }}
          >
            <Text
              style={{
                color: '#ffff',
                fontSize: 20,
                fontWeight: 'bold',
              }}
            >
              FakeMovies
            </Text>
          </View>
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 10,
          }}
          >
            <Button onPress={() => navigation.navigate('searchPage_menu')} transparent>
              <MainIcon
                iconType="search"
                iconSize={25}
                iconColor="#ffff"
              />
            </Button>
          </View>
        </Header>
        <Content>
          <FlatList
            data={data}
            // render per item
            renderItem={this.renderItem}
            // key list
            keyExtractor={item => item.id.toString()}
            // num columns for grid view
            numColumns={3}
          />

        </Content>
      </Container>
    );
  }
}

// props data type required for react navigation
browseMovie.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
