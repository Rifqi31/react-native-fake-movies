import React from 'react';
import {
  StatusBar,
  FlatList,
  Text,
} from 'react-native';
// import native base module
import {
  // base component
  Container,
  Content,
  ListItem,
  // component require
} from 'native-base';
// import proptypes for define prop data type
import PropTypes from 'prop-types';

export default class detailBrowseMovie extends React.Component {
  // create constructor
  constructor(props) {
    super(props);
    // initiate param or trigger from another page
    // destruction props
    const { navigation } = this.props;
    const item = navigation.state.params;
    // default statement
    this.state = {
      // depends on API
      // using API TmDB API
      data: [],
      apiVersion: 3,
      apiKey: '9a4a662a126525b07d4b84b079d809d8',
      language: 'en-US',
      // optional param
      sortBy: 'popularity.desc',
      includeAdultmovie: true,
      includeVideo: false,
      page: 1,
      // getting id
      genreId: item.id,
    };
  }

  // calling default font for native base config
  componentDidMount() {
    this.makeremoteRequest();
  }

  makeremoteRequest = () => {
    const {
      apiVersion,
      apiKey,
      language,
      sortBy,
      includeAdultmovie,
      includeVideo,
      page,
      genreId,
    } = this.state;
    const url = `https://api.themoviedb.org/${apiVersion}/discover/movie?api_key=${apiKey}&language=${language}&sort_by=${sortBy}&include_adult=${includeAdultmovie}&include_video=${includeVideo}&page=${page}&with_genres=${genreId}`;
    fetch(url)
      .then(response => response.json())
      .then((response) => {
        // filter data what we need in this page
        const originalData = response.results;
        const filterData = [];
        // eslint-disable-next-line array-callback-return
        originalData.map((data) => {
          let containData = {};
          const movieId = data.id;
          const movieTitle = data.title;
          const moviePopularity = data.popularity;
          const moviePoster = data.poster_path;
          const movieLanguage = data.original_language;
          const movieReleaseDate = data.release_date;

          const getData = {
            id: movieId,
            title: movieTitle,
            popularity: moviePopularity,
            poster: moviePoster,
            language: movieLanguage,
            release_date: movieReleaseDate,
          };
          containData = { ...containData, ...getData };
          filterData.push(containData);
        });
        this.setState({
          // depends on json structure if there is no results field use response only
          data: filterData,
        });
      });
  };

  // render data list
  renderItem = ({ item }) => (
    <ListItem>
      <Text>{item.title}</Text>
    </ListItem>
  );

  render() {
    const { data } = this.state;
    return (
      <Container>
        <StatusBar hidden />
        <Content>
          <FlatList
            data={data}
            // render per item
            renderItem={this.renderItem}
            // key list
            keyExtractor={item => item.id.toString()}
          />
        </Content>
      </Container>
    );
  }
}

// props data type required for react navigation
detailBrowseMovie.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
