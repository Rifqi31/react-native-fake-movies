import React from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  FlatList,
  Image
} from "react-native";
// import native base module
import {
  // base component
  Container,
  Content,
  // component require
  Icon,
  Left,
  Thumbnail,
  Button,
  ListItem
} from "native-base";
// import image overlay module
import ImageOverlay from "react-native-image-overlay";
// import another components
import FooterTab from "../components/footerTab";

export default class detailMovie extends React.Component {
  //   create constructor
  constructor(props) {
    super(props);
    // initiate param or trigger from another page
    const { item } = this.props.navigation.state.params;
    // default statement
    this.state = {
      loading: true,
      data: [],
      // depends on API
      // using API TmDB API
      api_version: 3,
      api_key: "9a4a662a126525b07d4b84b079d809d8",
      language: "en-US",
      // optional param
      movie_id: item.id,
      //
      error: null,
      refreshing: false
    };
  }

  // calling default font for native base config
  componentDidMount() {
    this.makeRemoteRequestCastingMovie();
  }

  // call the api url and manipulate it
  // request casting actors
  makeRemoteRequestCastingMovie = () => {
    const { api_version, api_key, movie_id } = this.state;
    const url = `https://api.themoviedb.org/${api_version}/movie/${movie_id}/credits?api_key=${api_key}`;
    this.setState({ loading: false });
    fetch(url)
      .then(response => response.json())
      .then(response => {
        // console.warn(response);
        this.setState({
          // depends on json structure if there is no results field use response only
          data: response.cast,
          error: response.error || null,
          loading: false
        });
        // for(x = 0; x <= response.cast.length; x++){
        // 	console.warn(response.cast[x].name);
        // }
        // console.warn(response.cast[0].name);
        // console.warn(response.cast.length);
      })

      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  // render movie item
  renderItem = ({ item }) => {
    return (
      // touchable item
      <ListItem style={{ borderBottomWidth: 0 }}>
        <Text>{item.name}</Text>
        <Thumbnail
          square
          source={{
            uri: "https://image.tmdb.org/t/p/original" + item.profile_path
          }}
        />
      </ListItem>
    );
  };

  render() {
    const { item } = this.props.navigation.state.params;
    return (
      <Container>
        <StatusBar hidden={true} />
        <Content>
          <ImageOverlay
            source={{
              uri: "https://image.tmdb.org/t/p/original" + item.poster_path
            }}
            contentPosition="bottom"
          >
            <View>
              <Text style={styles.titleMovie}>{item.title}</Text>
              <Text style={styles.detailMovie}>{item.release_date}</Text>
              <Text style={styles.detailMovie}>{item.vote_average}</Text>
            </View>
		  </ImageOverlay>

		  {/* image gradient */}
          {/* <Image
            resizeMode="cover"
            source={{
              uri: "https://image.tmdb.org/t/p/original" + item.poster_path
            }}
            style={{ width: "100%", height: 300 }}
          />

          <LinearGradient
            colors={["rgba(0,0,0,0.8)", "transparent"]}
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: 0,
              height: 300
            }}
          /> */}

          <View>
            <Text>Synopsis</Text>
            <Text>{item.overview}</Text>
          </View>
          <FlatList
            data={this.state.data}
            horizontal
            // render per item
            renderItem={this.renderItem}
            // key list
            keyExtractor={item => item.id.toString()}
          />
        </Content>
        <FooterTab />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  titleMovie: {
    color: "#ffff"
  },
  detailMovie: {
    color: "#B9B6BA"
  }
});
