// import react module
import React from 'react';
// import react native module
import {
  View, FlatList, Text, Image,
} from 'react-native';
// import native base module
import {
  Container, Header, Content, ListItem, Button, Card,
} from 'native-base';
// import proptypes for define prop data type
import PropTypes from 'prop-types';
// import custom icon
import MainIcon from '../../assets/icons/MainIcon';
// import custom loading
import LoadingPoster from '../components/loadingActivity';

// define and export class
export default class homePage extends React.Component {
  // create constructor
  constructor(props) {
    super(props);
    // default statement
    this.state = {
      dataMovies: [],
      dataTVSeries: [],
      dataAnimationSeries: [],
      dataMovieActions: [],
      // depends on API
      // using API TmDB API
      apiVersion: 3,
      apiKey: '9a4a662a126525b07d4b84b079d809d8',
      language: 'en-US',
      // optional param
      sortBy: 'popularity.desc',
      timezone: 'America%2FNew_York',
      includeAdultmovie: true,
      includeVideo: false,
      includeFirstAirDates: false,
      page: 1,
      // set loading data
      loadingData: false,
    };
  }

  // after render component call this function
  componentDidMount() {
    // call remote request API function
    this.makeRemoteRequest();
  }

  // create function to fect API and getting data from json
  makeRemoteRequest = () => {
    // set loading true
    this.setState({
      loadingData: true,
    });
    // call states from contsructor
    const {
      apiVersion,
      apiKey,
      language,
      sortBy,
      includeAdultmovie,
      includeVideo,
      includeFirstAirDates,
      page,
      timezone,
    } = this.state;

    // define url api
    const urlDiscoverMovie = `https://api.themoviedb.org/${apiVersion}/discover/movie?api_key=${apiKey}&language=${language}&sort_by=${sortBy}&include_adult=${includeAdultmovie}&include_video=${includeVideo}&page=${page}`;
    const urlDiscoverTV = `https://api.themoviedb.org/${apiVersion}/discover/tv?api_key=${apiKey}&language=${language}&sort_by=${sortBy}&page=${page}&timezone=${timezone}&include_null_first_air_dates=${includeFirstAirDates}`;
    const urlDiscoverAnime = `https://api.themoviedb.org/${apiVersion}/discover/tv?api_key=${apiKey}&language=${language}&sort_by=${sortBy}&page=${page}&timezone=${timezone}&with_genres=16&include_null_first_air_dates=${includeFirstAirDates}`;
    const urlDiscoverAction = `https://api.themoviedb.org/${apiVersion}/discover/movie?api_key=${apiKey}&language=${language}&sort_by=${sortBy}&include_adult=${includeAdultmovie}&include_video=${includeVideo}&page=${page}&with_genres=28`;

    // this.setState({ loading: true });
    // use Promise All for multiple API request
    Promise.all([
      fetch(urlDiscoverMovie),
      fetch(urlDiscoverTV),
      fetch(urlDiscoverAnime),
      fetch(urlDiscoverAction),
    ])
      // then create object for contain json for eact request API
      .then(([responseMovie, responseTV, responseAnime, responseAction]) => Promise.all([
        responseMovie.json(),
        responseTV.json(),
        responseAnime.json(),
        responseAction.json(),
      ]))
      // then create object for storing data into our state
      .then(([responseMovie, responseTV, responseAnime, responseAction]) => {
        // using ES6
        // responseMovie.results
        const newFilterDataMovies = [];
        // eslint-disable-next-line array-callback-return
        responseMovie.results.map((data) => {
          let newObj = {};
          let showTitle;
          const idMovie = data.id;
          const titleMovie = data.title;
          const posterMovie = data.poster_path;

          if (titleMovie.length > 15) {
            const modifyText = titleMovie.slice(0, 13);
            showTitle = `${modifyText}...`;
          } else {
            showTitle = titleMovie;
          }

          const newData = { id: idMovie, title: showTitle, poster_path: posterMovie };
          newObj = { ...newObj, ...newData };
          newFilterDataMovies.push(newObj);
        });
        // console.warn(newFilterData);
        // responsTV.results
        const newFilterDataTVs = [];
        // eslint-disable-next-line array-callback-return
        responseTV.results.map((data) => {
          let newObj = {};
          let showTitle;
          const idMovie = data.id;
          const nameTV = data.name;
          const posterTV = data.poster_path;

          if (nameTV.length > 15) {
            const modifyText = nameTV.slice(0, 13);
            showTitle = `${modifyText}...`;
          } else {
            showTitle = nameTV;
          }

          const newData = { id: idMovie, title: showTitle, poster_path: posterTV };
          newObj = { ...newObj, ...newData };
          newFilterDataTVs.push(newObj);
        });
        // console.warn(newFilterDataTVs);
        // responseAnime.results
        const newFilterDataAnimes = [];
        // eslint-disable-next-line array-callback-return
        responseAnime.results.map((data) => {
          let newObj = {};
          let showTitle;
          const idMovie = data.id;
          const nameAnime = data.name;
          const posterAnime = data.poster_path;
          const backDropMovie = data.backdrop_path;

          if (nameAnime.length > 15) {
            const modifyText = nameAnime.slice(0, 13);
            showTitle = `${modifyText}...`;
          } else {
            showTitle = nameAnime;
          }

          const newData = {
            id: idMovie, name: showTitle, poster_path: posterAnime, backdrop_path: backDropMovie,
          };
          newObj = { ...newObj, ...newData };
          newFilterDataAnimes.push(newObj);
        });
        // console.warn(newFilterDataAnimes);
        // responseAction.results
        const newFilterDataActionMovies = [];
        // eslint-disable-next-line array-callback-return
        responseAction.results.map((data) => {
          let newObj = {};
          let showTitle;
          const idMovie = data.id;
          const titleMovieActions = data.title;
          const posterMovies = data.poster_path;

          if (titleMovieActions.length > 15) {
            const modifyText = titleMovieActions.slice(0, 13);
            showTitle = `${modifyText}...`;
          } else {
            showTitle = titleMovieActions;
          }

          const newData = { id: idMovie, title: showTitle, poster_path: posterMovies };
          newObj = { ...newObj, ...newData };
          newFilterDataActionMovies.push(newObj);
        });
        // console.warn(newFilterDataActionMovies);
        this.setState({
          // store into state data
          dataMovies: newFilterDataMovies,
          dataTVSeries: newFilterDataTVs,
          dataAnimationSeries: newFilterDataAnimes,
          dataMovieActions: newFilterDataActionMovies,
          // set loading false
          loadingData: false,
        });
      });
  };

  // render item list
  // create arrow function with 'item' param
  renderItem = ({ item }) => {
    const { navigation } = this.props;
    return (
      // touchable item
      <ListItem
        style={{ borderBottomWidth: 0, paddingRight: 0 }}
        Thumbnail
        onPress={() => navigation.navigate('detailPage_menu', { item })
          }
      >
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <Card>
            <Image
              style={{
                height: 150,
                width: 100,
              }}
              source={{
                uri: `https://image.tmdb.org/t/p/w154${item.poster_path}`,
              }}
            />
            <Text style={{ textAlign: 'center' }}>{item.title}</Text>
          </Card>
        </View>
      </ListItem>
    );
  };

  // render item list with large poster
  // create arrow function with 'item' param
  renderItemLargePoster = ({ item }) => {
    // destruction 'navigation' props
    const { navigation } = this.props;
    return (
      // touchable item
      <ListItem
        style={{ borderBottomWidth: 0, paddingRight: 0 }}
        Thumbnail
        onPress={() => navigation.navigate('detailPage_menu', { item })
        }
      >
        <Image
          style={{
            height: 150,
            width: 250,
          }}
          source={{
            uri: `https://image.tmdb.org/t/p/w300${item.backdrop_path}`,
          }}
        />
      </ListItem>
    );
  };

  render() {
    // destruction 'navigation' props
    const { navigation } = this.props;
    // destruction states
    const {
      dataMovies, dataTVSeries, dataAnimationSeries, dataMovieActions, loadingData,
    } = this.state;
    return (
      <Container>
        <Header androidStatusBarColor="#151A46" style={{ backgroundColor: '#151A46' }}>
          {/* Text Title */}
          <View style={{
            flex: 1,
            justifyContent: 'flex-start',
            flexDirection: 'row',
          }}
          />
          <View style={{
            marginVertical: 10,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
          }}
          >
            <Text
              style={{
                color: '#ffff',
                fontSize: 20,
                fontWeight: 'bold',
              }}
            >
              FakeMovies
            </Text>
          </View>
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 10,
          }}
          >
            <Button onPress={() => navigation.navigate('searchPage_menu')} transparent>
              <MainIcon
                iconType="search"
                iconSize={25}
                iconColor="#ffff"
              />
            </Button>
          </View>
        </Header>
        <Content>
          <View>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '200',
                color: '#000',
              }}
            >
              HightLight Movies
            </Text>
            {/* image slide show */}
            {loadingData
              ? (
                <LoadingPoster posterType="normal" />
              )
              : (
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataMovies}
                  // render per item
                  renderItem={this.renderItem}
                  // key list
                  keyExtractor={item => item.id.toString()}
                />
              )}

            <Text
              style={{
                fontSize: 18,
                fontWeight: '200',
                color: '#000',
              }}
            >
              Animation Series
            </Text>
            {/* image slide show */}
            {loadingData
              ? (
                <LoadingPoster posterType="large" />
              )
              : (
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataAnimationSeries}
                // render per item
                  renderItem={this.renderItemLargePoster}
                // key list
                  keyExtractor={item => item.id.toString()}
                />
              )}

            <Text
              style={{
                fontSize: 18,
                fontWeight: '200',
                color: '#000',
              }}
            >
              HightLight TV Series
            </Text>
            {/* image slide show */}
            {loadingData
              ? (
                <LoadingPoster posterType="normal" />
              )
              : (<FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={dataTVSeries}
                // render per item
                renderItem={this.renderItem}
                // key list
                keyExtractor={item => item.id.toString()}
              />
              )}

            <Text
              style={{
                fontSize: 18,
                fontWeight: '200',
                color: '#000',
              }}
            >
              Action Movies
            </Text>
            {/* image slide show */}
            {loadingData
              ? (
                <LoadingPoster posterType="normal" />
              )
              : (
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={dataMovieActions}
                // render per item
                  renderItem={this.renderItem}
                // key list
                  keyExtractor={item => item.id.toString()}
                />
              )}
          </View>
        </Content>
      </Container>
    );
  }
}

// props data type required for react navigation
homePage.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
