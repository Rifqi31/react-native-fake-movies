import React from 'react';
import { StyleSheet, View } from 'react-native';
// import native base module
import {
	Container,
	Header,
	Left,
	Right,
	Title,
	Text,
	Content,
	// Footer
} from 'native-base';
// import another components
import FooterTab from '../components/footerTab';

export default class profileUser extends React.Component {

	// calling default font for native base config
	// componentDidMount() {
	// }

	render() {
		return (
			<View style={styles.container}>
				<Text>this is profileUser view!</Text>
				<FooterTab />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
