import React from "react";
import { StyleSheet, View } from "react-native";
// configure for native base in expo
import { Font } from "expo";
import { Ionicons } from "@expo/vector-icons";
// import native base module
import {
  Container,
  Header,
  Title,
  Text,
  Content,
  Input,
  Button
} from "native-base";

export default class registerPage extends React.Component {
  // calling default font for native base config
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      ...Ionicons.font
    });
  }

  render(){
      return(
          <Container>
              <Content>

              </Content>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center"
    }
});
