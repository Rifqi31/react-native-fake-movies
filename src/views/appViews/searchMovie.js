import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
// import native base module
import {
  Container,
  Header,
  Left,
  Right,
  Title,
  Text,
  Content,
  Input,
  Button,
  Icon,
  ListItem,
  Thumbnail,
  Item
  // Footer
} from "native-base";
// import another components
import FooterTab from "../components/footerTab";

export default class searchMovie extends React.Component {
  // constuctor
  constructor(props) {
    super(props);

    // default statement
    this.state = {
      loading: false,
      data: [],
      // depends on API
      // using API TmDB API
      api_version: 3,
      api_key: "9a4a662a126525b07d4b84b079d809d8",
      language: "en-US",
      // optional param
      sort_by: "popularity.desc",
      include_adult_movie: true,
      include_video: false,
      page: 1,
      //
      error: null,
      refreshing: false
    };
    this.arrayHolder = [];
  }

  // calling default font for native base config
  componentDidMount() {
    this.makeRemoteRequestSearchMovies();
  }

  makeRemoteRequestSearchMovies = () => {
    const {
      api_version,
      api_key,
      language,
      sort_by,
      include_adult_movie,
      include_video,
      page
    } = this.state;
    const url = `https://api.themoviedb.org/${api_version}/discover/movie?api_key=${api_key}&language=${language}&sort_by=${sort_by}&include_adult=${include_adult_movie}&include_video=${include_video}&page=${page}`;
    this.setState({ loading: true });
    fetch(url)
      .then(response => response.json())
      .then(response => {
        // console.warn(response.results);
        this.setState({
          data: response.results,
          error: response.error || null,
          loading: false
          // refreshing: false
        });

        this.arrayHolder = response.results;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  //   renderHeaderSearchBar = () => {
  //     return (
  //       <Header searchBar rounded>
  //         <Item>
  //           <Icon name="ios-search" />
  //           <Input
  //             placeholder="Search"
  //             onChangeText={text => this.searchFilterFunction(text)}
  // 			autoCorrect={false}
  //           />
  //         </Item>
  //         <Button transparent>
  //           <Text>Search</Text>
  //         </Button>
  //       </Header>
  //     );
  //   };

  searchFilterFunction = text => {
    const newData = this.arrayHolder.filter(item => {
      const itemData = `${item.title.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

  // render movie item
  renderItem = ({ item }) => {
    return (
      // touchable item
      <ListItem
        style={{ borderBottomWidth: 0 }}
        Thumbnail
        onPress={() =>
          this.props.navigation.navigate("detailPage_menu", { item })
        }
      >
        <Thumbnail
          style={{
            height: 350,
            width: 250,
            borderRadius: 30 / 2
          }}
          square
          large
          source={{
            uri: "https://image.tmdb.org/t/p/original" + item.poster_path
          }}
        />
      </ListItem>
    );
  };

  listEmpty = () => {
	  return(
		  <View>
			  <Text>Nothing to show about it!</Text>
		  </View>
	  )
  }

  render() {
    // console.warn(this.state.data);
    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              onChangeText={text => this.searchFilterFunction(text)}
              autoCorrect={false}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
        <Content>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <FlatList
              data={this.state.data}
              // render per item
              renderItem={this.renderItem}
              // key list
			  keyExtractor={item => item.id.toString()}
			  ListEmptyComponent={this.listEmpty}
			/>
          </View>
        </Content>
        <FooterTab />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
