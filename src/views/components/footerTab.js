// import react module
import React from 'react';
// import react native module
import { View, Text, StyleSheet } from 'react-native';
// import proptypes for define prop data type
import PropTypes from 'prop-types';
// import native base module
import { Footer, FooterTab, Button } from 'native-base';
// fixing navigation
import { withNavigation } from 'react-navigation';
// import custom icon
import MainIcon from '../../assets/icons/MainIcon';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#353848',
  },
  textColor: {
    color: '#B9C2C2',
  },
});

// create class defautl
class footerTab extends React.Component {
  customFooterTab = () => {
    // destruction 'navigation' props
    const { navigation } = this.props;
    return (
      <Footer>
        <FooterTab style={styles.container}>
          <Button vertical onPress={() => navigation.navigate('homePage_menu')}>
            <MainIcon iconType="discover" iconSize={25} iconColor="#ffff" />
            <Text style={styles.textColor}>Discover</Text>
          </Button>
          <Button vertical onPress={() => navigation.navigate('browsePage_menu')}>
            <MainIcon iconType="browse" iconSize={25} iconColor="#ffff" />
            <Text style={styles.textColor}>Browse</Text>
          </Button>
          <Button vertical onPress={() => navigation.navigate('profilePage_menu')}>
            <MainIcon iconType="library" iconSize={25} iconColor="#ffff" />
            <Text style={styles.textColor}>Library</Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }

  render() {
    return (
      <View>
        {this.customFooterTab() }
      </View>
    );
  }
}

// props data type required for react navigation
footerTab.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default withNavigation(footerTab);
