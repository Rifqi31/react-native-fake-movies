// import react module
import React from 'react';
// import react native module
import { View } from 'react-native';
// import rn placeholder
import { connect } from 'rn-placeholder';
// import proptypes for define prop data type
import PropTypes from 'prop-types';

const containLoad = [];
export default class loadingActivity extends React.Component {
  normalPosterLoading = () => {
    const NormalimageHoc = connect(() => (
      <View style={{
        backgroundColor: '#D3D3D3',
        height: 150,
        width: 100,
        marginLeft: 10,
      }}
      />
    ));
    for (let indexComp = 0; indexComp < 4; indexComp += 1) {
      const callLoadImage = (<NormalimageHoc animation="fade" key={indexComp} />);
      containLoad[indexComp] = (callLoadImage);
    }
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {containLoad}
      </View>
    );
  }

  largePosterLoading = () => {
    const LargeimageHoc = connect(() => (
      <View style={{
        backgroundColor: '#D3D3D3',
        height: 150,
        width: 250,
        marginLeft: 10,
      }}
      />
    ));
    for (let indexComp = 0; indexComp < 4; indexComp += 1) {
      const callLoadImage = (<LargeimageHoc animation="fade" key={indexComp} />);
      containLoad[indexComp] = (callLoadImage);
    }
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {containLoad}
      </View>
    );
  }

   callPosterLoadingActivity = () => {
     const { posterType } = this.props;
     const posterTypeName = ['normal', 'large'];
     let containPosterType;

     if (posterType === posterTypeName[0]) {
       containPosterType = this.normalPosterLoading();
     }
     if (posterType === posterTypeName[1]) {
       containPosterType = this.largePosterLoading();
     }

     return containPosterType;
   }

   render() {
     return (
       <View>
         {this.callPosterLoadingActivity()}
       </View>
     );
   }
}

// set props data type
loadingActivity.propTypes = {
  posterType: PropTypes.string,
};

loadingActivity.defaultProps = {
  posterType: 'none',
};
